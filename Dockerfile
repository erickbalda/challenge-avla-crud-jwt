FROM openjdk:11-jdk-slim
EXPOSE 8080
RUN mkdir -p /app/
ADD build/libs/avla-0.0.1-SNAPSHOT.jar /app/avla-challenge.jar
ENTRYPOINT ["java", "-jar", "/app/avla-challenge.jar"]