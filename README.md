# Avla Challenge

## Version Backend

- Spring Boot 2.7.11
- Swagger 3.0.0

## Requerimientos Minimos

- Java 11
- Gradle 8.1.1

## Database

- In-Memory Database

## Ejecutar proyecto con Gradle

1. Descargar proyecto

2. Abrir terminal en la ruta principal del proyecto

3. Ejecutar el siguiente comando

            gradlew bootRun

### Ejecutar proyecto con Docker

1. Descargar proyecto

2. Abrir terminal en la ruta principal del proyecto

3. ejecutar comando para construccion de imagen

        docker build -t avla-challenge .

4. Ejecutar el contenedor

        docker run -p 8080:8080 avla-challenge

## Testing Backend

1. una vez ejecutado el sistema se genera documentacion y testing con Swagger, se puede consultar a traves de la URL <http://localhost:8080/swagger-ui.html>

2. Se anexa en la ruta principal del proyecto archivo para importacion de prueba de endpoints y Test en programa POSTMAN .

## Instrucciones

 Desarrollar un servicio API REST, en el cual se deben crear endpoints correspondientes a un CRUD de usuarios, los endpoint deben recibir y responder sólo en formato JSON.

Formato mensaje:

            {"mensaje": "mensaje de error"}
            {"mensaje": "usuario eliminado correctamente."}

#### Registro y actualización de usuarios

- El endpoint de creación y actualización de usuarios, deberá recibir un body con los campos "name", "email", "password", más un listado del objeto "phones", respetando el siguiente formato:

        {
            "name": "Rodrigo Paredes",
            "email": "rodrigo.paredes@email.com",
            "password": "Password12",
            "phones": [
                        {
                        "number": "987654321",
                        }
                    ]
        }
- Responder el código de status HTTP correspondiente..
- En caso de registrar o actualizar correctamente el usuario, debe retornar los siguientes campos:

            id: id del usuario.
            created: fecha de creación del usuario.
            modified: fecha de la última actualización de usuario.
            token: token de acceso de la API (JWT).
- En el caso de que el correo del usuario se encuentre registrado, deberá retornar el siguiente error "El correo ya se encuentra registrado".
- El correo debe ser validado por una expresión regular.
- La clave debe ser validada por una expresión regular con el siguiente formato. (Una mayúscula, letras minúsculas, y dos números) .
- Se debe implementar logs en cada endpoint.
- El token debe ser persistido al momento de registrar el usuario.
- Se debe agregar documentación de la API construida, por ejemplo con swagger.

#### Requisitos mandatorios

- Base de datos en memoria.
- Framework Spring Boot.
- Java 11.
- Gradle.
- Entrega en un repositorio público (github, gitlab o bitbucket) con el código fuente.
- README.md debe contener las instrucciones para levantar el proyecto.
  
#### Requisitos deseables

- token con JWT
- Docker con la solución
