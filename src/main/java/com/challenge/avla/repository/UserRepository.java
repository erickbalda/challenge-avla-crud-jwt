package com.challenge.avla.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.avla.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    void deleteByEmail(String email);
}
