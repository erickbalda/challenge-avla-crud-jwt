package com.challenge.avla.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.avla.entities.User;
import com.challenge.avla.model.UserRequest;
import com.challenge.avla.repository.UserRepository;
import com.challenge.avla.utils.Messages;

@Service
public class ValidationService {
    
    @Autowired
    private UserRepository userRepository;
    
    public void validateExists(UserRequest user) {
        User userFind = userRepository.findByEmail(user.getEmail());
        Optional<User> userExists = Optional.ofNullable(userFind); 
        if (userExists.isPresent()) {
                throw new IllegalArgumentException(Messages.EMAIL_ALREADY_EXISTS);
        }
    }

    public void validateMail(UserRequest user) {
        if(!user.getEmail().matches("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$")) { 
            throw new IllegalArgumentException(Messages.EMAIL_INVALID);
        }
    }

    public void validatePassword(UserRequest user) {
        if(!user.getPassword().matches("^(?=.*[A-Z])(?=.*[a-z])(?=.*[\\d]{2}).{6,}$")) { 
            throw new IllegalArgumentException(Messages.PASSWORD_INVALID);
        }
    }
}
