package com.challenge.avla.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.challenge.avla.entities.User;
import com.challenge.avla.model.UserRequest;
import com.challenge.avla.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private ValidationService validationService;
    
    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    @Transactional()
    public User save(UserRequest user) {
        validationService.validateExists(user);
        validationService.validateMail(user);
        validationService.validatePassword(user);
        User newUser = new User();
        newUser.setName(user.getName());
        newUser.setEmail(user.getEmail());
        newUser.setPassword(user.getPassword());
        newUser.setPhones(user.getPhones());
        newUser.setToken(jwtService.generateToken(user));
    	return userRepository.save(newUser);
    }


    @Override
    @Transactional()
    public User update(User user, UserRequest userUpdate) {
        validationService.validateMail(userUpdate);
        validationService.validatePassword(userUpdate);
        user.setName(userUpdate.getName());
        user.setEmail(userUpdate.getEmail());
        user.setPassword(userUpdate.getPassword());
        user.setPhones(userUpdate.getPhones());
        user.setModified(new Date());
        user.setToken(jwtService.generateToken(userUpdate));
    	return userRepository.save(user);
    }

    @Override
    @Transactional()
    public void deleteByEmail(String email) {
        userRepository.deleteByEmail(email);
    }

}
