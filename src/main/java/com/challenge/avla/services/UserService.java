package com.challenge.avla.services;

import java.util.List;

import com.challenge.avla.entities.User;
import com.challenge.avla.model.UserRequest;

public interface UserService {
	
	public List<User> findAll();
    public User findByEmail(String email);
	public User save(UserRequest user);    
    public User update(User user, UserRequest userUpdate);  
    public void deleteByEmail(String email);
}
