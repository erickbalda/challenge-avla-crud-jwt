package com.challenge.avla.services;

import org.springframework.stereotype.Service;

import com.challenge.avla.model.UserRequest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.util.Date;

import javax.crypto.SecretKey;

@Service
public class JwtService {

    public String generateToken(UserRequest user) {
        SecretKey jwtSecret = Keys.secretKeyFor(SignatureAlgorithm.HS512);
        Date now = new Date();
        Date expireDate = new Date(now.getTime() + 86400000L);

        Claims claims = Jwts.claims();
        claims.put("data", user);
        
        return Jwts.builder()
                .setClaims(claims)
                .setSubject("avlaToken")
                .setIssuedAt(now)
                .setExpiration(expireDate)
                .signWith(jwtSecret)
                .compact();
    }
}
