package com.challenge.avla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvlaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvlaApplication.class, args);
	}

}
