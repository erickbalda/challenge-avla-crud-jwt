package com.challenge.avla.utils;

public class Messages {
    public static final String UPDATE_USER = "Usuario actualizado";
    public static final String DELETE_USER = "Usuario eliminado";
    public static final String CREATE_USER = "Usuario creado";
    public static final String USER_NOT_FOUND = "Usuario no encontrado.";
    public static final String USER_DELETED = "Usuario eliminado correctamente.";
    public static final String EMAIL_ALREADY_EXISTS = "El correo ya se encuentra registrado.";
    public static final String EMAIL_INVALID = "El formato del correo electrónico no es válido.";
    public static final String PASSWORD_INVALID = "La contraseña debe contener una mayúscula, letras minúsculas y dos números";
    public static final String INTERNAL_ERROR = "Error interno del servidor";
    public static final String BAD_REQUEST = "Peticion erronea";

    private Messages() {
        throw new IllegalStateException("Utility class");
      }
}
