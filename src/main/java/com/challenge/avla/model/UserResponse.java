package com.challenge.avla.model;

import java.util.Date;

import com.challenge.avla.entities.User;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Response para metodo GET y UPDATE")
public class UserResponse {

    private Long id;
    private Date created;
    private Date modified;
    private String token;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }
    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public UserResponse(User user) {
        this.id = user.getId();
        this.created = user.getCreated();
        this.modified = user.getModified();
        this.token = user.getToken();
    }


    public UserResponse(Long id, Date created, Date modified, String token) {
        this.id = id;
        this.created = created;
        this.modified = modified;
        this.token = token;
    }
    @Override
    public String toString() {
        return "UserResponse [id=" + id + ", created=" + created + ", modified=" + modified + ", token=" + token + "]";
    }
 

}
