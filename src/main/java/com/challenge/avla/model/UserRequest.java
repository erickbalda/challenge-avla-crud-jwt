package com.challenge.avla.model;

import java.util.List;

import com.challenge.avla.entities.Phone;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Modelo de datos recibido por la peticion")
public class UserRequest {

    private String name;
    private String email;
    private String password;
    private List<Phone> phones;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public List<Phone> getPhones() {
        return phones;
    }
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }
    public UserRequest(String name, String email, String password, List<Phone> phones) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phones = phones;
    }
 

}
