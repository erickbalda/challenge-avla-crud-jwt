package com.challenge.avla.model;

import io.swagger.v3.oas.annotations.media.Schema;

public class MessageResponse {
    
    @Schema(description = "Mensaje simple para response")
    private String msg;

    public MessageResponse(String msg) {
        this.msg = msg;
    }
    public String getmsg() {
        return msg;
    }
    public void setmsg(String msg) {
        this.msg = msg;
    }

}
