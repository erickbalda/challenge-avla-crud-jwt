package com.challenge.avla.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.avla.entities.User;
import com.challenge.avla.model.MessageResponse;
import com.challenge.avla.model.UserRequest;
import com.challenge.avla.model.UserResponse;
import com.challenge.avla.services.UserService;
import com.challenge.avla.utils.Messages;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "User Controller")
@RestController
@RequestMapping("/v1/api")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Operation(summary = "Metodo GET para obtencion de usuarios registrados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Listado de usuarios registrados", content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))) })
    @GetMapping("/users")
    public List<User> getAllUsers() {
        logger.info("Listado de usuarios solicitado");
        return userService.findAll();
    }

    @Operation(summary = "Metodo POST para la creacion de nuevo usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = Messages.CREATE_USER, content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserResponse.class))),
            @ApiResponse(responseCode = "400", description = Messages.BAD_REQUEST, content = @Content(schema = @Schema(implementation = MessageResponse.class))),
            @ApiResponse(responseCode = "500", description = Messages.INTERNAL_ERROR, content = @Content(schema = @Schema(implementation = MessageResponse.class))) })
    @PostMapping("/user")
    public ResponseEntity<?> createUser(@RequestBody UserRequest user) {
        try {
            User savedUser = userService.save(user);
            UserResponse userResponse = new UserResponse(savedUser);
            logger.info(Messages.CREATE_USER);
            return ResponseEntity.status(HttpStatus.CREATED).body(userResponse);
        } catch (IllegalArgumentException e) {
            logger.warn(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            logger.warn(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }

    }

    @Operation(summary = "Metodo PUT para la actualizacion de datos de un usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = Messages.UPDATE_USER, content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserResponse.class))),
            @ApiResponse(responseCode = "404", description = Messages.USER_NOT_FOUND, content = @Content(schema = @Schema(implementation = MessageResponse.class))) })
    @PutMapping("/user")
    public ResponseEntity<?> update(@RequestBody UserRequest user) {
        User userFind = userService.findByEmail(user.getEmail());
        Optional<User> userExists = Optional.ofNullable(userFind);

        if (userExists.isPresent()) {
            User savedUser = userService.update(userFind, user);
            UserResponse userResponse = new UserResponse(savedUser);
            logger.info(Messages.UPDATE_USER);
            return ResponseEntity.status(HttpStatus.OK).body(userResponse);
        } else {
            logger.warn(Messages.USER_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Messages.USER_NOT_FOUND);
        }
    }

    @Operation(summary = "Metodo DELETE para la eliminacion de usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = Messages.DELETE_USER, content = @Content(mediaType = "application/json", schema = @Schema(implementation = MessageResponse.class))),
            @ApiResponse(responseCode = "404", description = Messages.USER_NOT_FOUND, content = @Content(schema = @Schema(implementation = MessageResponse.class))) })
    @DeleteMapping("/user")
    public ResponseEntity<String> delete(@RequestBody UserRequest user) {
        User userFind = userService.findByEmail(user.getEmail());
        Optional<User> userExists = Optional.ofNullable(userFind);

        if (userExists.isPresent()) {
            userService.deleteByEmail(user.getEmail());
            logger.info(Messages.DELETE_USER, user);
            return ResponseEntity.status(HttpStatus.OK).body(Messages.USER_DELETED);
        } else {
            logger.warn(Messages.USER_NOT_FOUND);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Messages.USER_NOT_FOUND);
        }
    }

}
